# Trees in Düsseldorf, Germany
This package is an analysis of cut-down trees in Düsseldorf, Germany 
during 2018/19. Build the vignette for results or visit 
[the corresponding blog post](https://hohenfeld.is/posts/cut-down-trees-in-d%C3%BCsseldorf-germany/).

## Licence 
The package, except the data, is available under the GNU GPL v3.

## Data Source
The data is taken from the city of Düsseldorf's open data page at 
https://opendata.duesseldorf.de/

The data is available under the Data Licence Germany 2.0, which you can find
under https://www.govdata.de/dl-de/zero-2-0