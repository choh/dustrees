describe("find containing geo", {
    beforeTest <- function() {
        point1 <- c(10, 10)
        point2 <- c(200, 250)
        poly1 <- sf::st_polygon(list(
            rbind(c(0, 0), c(0, 20), c(20, 20), c(20, 0), c(0, 0))
        ))
        poly2 <- sf::st_polygon(list(
            rbind(c(50, 50), c(50, 100), c(100, 100), c(100, 50), c(50, 50))
        ))
        poly3 <- sf::st_polygon(list(
            rbind(c(0, 0), c(0, 30), c(30, 30), c(30, 0), c(0, 0))
        ))
        geodata <- sf::st_sf(
            name = c("poly1", "poly2"),
            geometry = c(sf::st_sfc(poly1), sf::st_sfc(poly2))
        )
        geodata2 <- sf::st_sf(
            name = c("poly1", "poly2", "poly3"),
            geometry = c(sf::st_sfc(poly1), sf::st_sfc(poly2),
                         sf::st_sfc(poly3))
        )
        list(
            point1 = point1,
            point2 = point2,
            geodata = geodata,
            geodata2 = geodata2
        )
    }

    it("returns the polygons a point is in", {
        testdata <- beforeTest()
        res <- find_containing_geo(testdata$geodata, point = testdata$point1)
        expect_equal(nrow(res), 1)
        expect_s3_class(res, "sf")
        res2 <- find_containing_geo(testdata$geodata2, point = testdata$point1)
        expect_equal(nrow(res2), 2)
        expect_s3_class(res2, "sf")
    })

    it("returns an empty simple features collection if point not in polygon", {
        testdata <- beforeTest()
        res <- find_containing_geo(testdata$geodata, point = testdata$point2)
        expect_equal(nrow(res), 0)
        expect_s3_class(res, "sf")
    })

    it("returns matches from specified column if it is given", {
        testdata <- beforeTest()
        res <- find_containing_geo(testdata$geodata2, "name", testdata$point1)
        expect_length(res, 2)
        expect_type(res, "character")
    })

    it("returns NA if column is specified but there is no match", {
        testdata <- beforeTest()
        res <- find_containing_geo(testdata$geodata, "name", testdata$point2)
        expect_equal(res, NA)
        expect_length(res, 1)
    })

    it("fails if point is not of length 2", {
        testdata <- beforeTest()
        expect_error(find_containing_geo(testdata$geodata, point = 1),
                     "Point must be lat and lon")
        expect_error(find_containing_geo(testdata$geodata, point = c(1, 2, 3)),
                     "Point must be lat and lon")
    })

    it("fails if specified column is not in data", {
        testdata <- beforeTest()
        expect_error(find_containing_geo(
            testdata$geodata, "city", testdata$point1),
            "Specified column not in in_area")
    })
})