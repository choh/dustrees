#' Data on cut-down trees in Düsseldorf, Germany
#'
#' The dataset included with this package contains information on cut-down
#' trees in Düsseldorf, Germany.
#' The souece of the original data is the open data website of the city
#' of Düsseldorf and the data is under the permissive Data Licence Germany 2.0.
#'
#' Being orignally four files the data has been merged together and lightly
#' cleaned.
#' The data has 2190 rows and 9 columns.
#' The colums are as follows:
#'
#' * lat: latitude
#' * lon: longitude
#' * reason: the reason why the tree has been cut down (in German)
#' * id: The id of the tree
#' * species: the species of the tree (in Germany)
#' * circ: the circumference of the tree in cm
#' * year: the year the tree was cut down in
#' * part: indicated whether the tree was cut down in summer (1) or winter (2)
#'
#' @name trees
#' @docType data
#' @keywords data
NULL