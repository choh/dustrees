average_circumference <- function(vec) {
#' Take a colum of circumferences and handle range values.
#'
#' Range values like 100/120 or 90-100 are averaged, while singular values
#' like 105 are returned as is.
#'
#' @param vec A vector of values.
#'
#' @return A vector of numeric values where ranges are averaged.
#' @export
    if (all(is.na(vec))) {
        return(vec)
    }
    x <- strsplit(vec, split = "[[:punct:]]")
    x <- sapply(lapply(x, as.numeric), mean, na.rm = TRUE, USE.NAMES = FALSE)
    x[is.nan(x)] <- NA_integer_
    x
}