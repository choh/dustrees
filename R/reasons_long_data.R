reasons_long_data <- function(wide_data, ...) {
#' Turn data with one-hot encoded "reason" column into long data.
#'
#' @param wide_data A dataset (data.frame or tbl) in wide format.
#' @param ... Column names to keep in the long data.
#'
#' @return The same dataset transformed to a long dataset where reason
#' is turned into a text column.
#'
#' @importFrom rlang .data
#'
#' @export
    keepvar <- rlang::enquos(...)
    valsym <- rlang::sym("value")
    namesym <- rlang::sym("name")
    longdata <- wide_data %>%
        dplyr::select(!!!keepvar, dplyr::starts_with("reason_")) %>%
        tidyr::pivot_longer(dplyr::starts_with("reason_")) %>%
        dplyr::mutate(name = gsub("reason_", "", .data$name)) %>%
        dplyr::rename(reason = !!namesym) %>%
        dplyr::filter(!!valsym == 1) %>%
        dplyr::select(-!!valsym)

    longdata
}