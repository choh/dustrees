library("dplyr")
library("here")
library("janitor")
library("readr")

# note that the average_circumference function is bundled with the package,
# so for generating the data for the package you need to load the package,
# e.g. using devtools

# ----- setup paths and read data ----------------------------------------------
basepath <- here()
data_raw_path <- file.path(basepath, "data-raw")

de_dot <- locale("de", decimal_mark = ".")

trees_2018_summer <- read_delim(
    file = file.path(data_raw_path, "Baumfällungen 2018 geocodiert.csv"),
    delim = ";", locale = de_dot)

trees_2018_winter <- read_delim(
    file = file.path(data_raw_path, "Winterfällung 2018_2019_2.csv"),
    delim = ";", locale = de_dot)

trees_2019_summer <- read_delim(
    file = file.path(data_raw_path, "Sommerfällungen 2019_0.csv"),
    delim = ";", locale = de_dot)

trees_2019_winter <- read_delim(
    file = file.path(data_raw_path, "Winterfällungen 2019_2020_1.csv"),
    delim = ";", locale = de_dot)

# ----- clean summer 2018 data -------------------------------------------------
trees_2018_summer <- clean_names(trees_2018_summer)

# drop columns not in other sets
trees_2018_summer <- trees_2018_summer %>%
    select(-altitude, -geometry, -art, -sorte, -gattung, -hausnummer,
           -pflegebezirk, -politischer_bezirk, -x_koord, -y_koord)

trees_2018_summer <- trees_2018_summer %>%
    mutate(baum_id = as.character(baum_id))

colnames(trees_2018_summer) <- c("lat", "lon", "reason", "id", "species_de",
                                 "street", "circ")

# ----- clean winter 2018 data -------------------------------------------------
trees_2018_winter <- clean_names(trees_2018_winter)
trees_2018_winter <- trees_2018_winter %>%
    mutate(stammumfang = average_circumference(stammumfang),
           baum_id = as.character(baum_id))

colnames(trees_2018_winter) <- c("id", "street", "species_de", "circ",
                                 "reason", "lat", "lon")


# ----- clean summer 2019 data -------------------------------------------------
trees_2019_summer <- clean_names(trees_2019_summer, case = "snake")
trees_2019_summer <- trees_2019_summer %>%
    mutate(stammumfang = average_circumference(stammumfang),
           baum_id = as.character(baum_id))

# drop columns not in other parts of the data
trees_2019_summer <- trees_2019_summer %>%
    select(-rechtswert, -hochwert)

colnames(trees_2019_summer) <- c("lon", "lat", "id", "street", "species_de",
                                 "circ", "reason")

# ----- clean winter 2019 data -------------------------------------------------
trees_2019_winter <- clean_names(trees_2019_winter, case = "snake")

# as we are dealing with data on a local level here it is safe to
# assume that coordinates are fairly limited, so we remove all . first
trees_2019_winter <- trees_2019_winter %>%
    mutate(latitude = gsub("\\.", "", latitude),
           longitude = gsub("\\.", "", longitude)) %>%
    mutate(latitude = gsub("^51", "51.", latitude),
           longitude = gsub("^6", "6.", longitude)) %>%
    mutate(latitude = as.numeric(latitude),
           longitude = as.numeric(longitude))

# now we have the circumference of the trees. some rows contain two values
# which we will average
trees_2019_winter <- trees_2019_winter %>%
    mutate(stammumfang = average_circumference(stammumfang))

colnames(trees_2019_winter) <- c("id", "street", "species_de", "circ",
                                 "reason", "lat", "lon")

# ----- add time indicators ----------------------------------------------------
trees_2018_summer <- trees_2018_summer %>%
    mutate(year = 2018, part = 1)

trees_2018_winter <- trees_2018_winter %>%
    mutate(year = 2018, part = 2)

trees_2019_summer <- trees_2019_summer %>%
    mutate(year = 2019, part = 1)

trees_2019_winter <- trees_2019_winter %>%
    mutate(year = 2019, part = 2)

# ----- bind together ----------------------------------------------------------
trees <- bind_rows(trees_2018_summer, trees_2018_winter,
                   trees_2019_summer, trees_2019_winter)

# ----- finally clean the tree names -------------------------------------------
trees <- trees %>%
    mutate(species_de = tolower(species_de))

# first handle multiple tree types in one entry
trees[grepl("[\\.,]", trees$species_de), "species_de"] <- "diverse"

# now handle cases where many names for one type of tree are apparently in
# one entry
# and also handle superfluous whitespace
trees <- trees %>%
    mutate(species_de = gsub(" - \\w+", "", species_de)) %>%
    mutate(species_de = trimws(species_de))

# drop one rouge numner and get rid of dashes
trees <- trees %>%
    mutate(species_de = gsub("120", NA, species_de)) %>%
    mutate(species_de = gsub("-", "", species_de))

# and finally handle different spellings and weird pasted together values
tree_key <- c(
    `abendlaendischer lebensbaum` = "abendländischer lebensbaum",
    baumweideweide = "baumweide",
    buchen = "buche",
    feldahornahorn = "feldahorn",
    `gemeine ebereschevogelbeerbau` = "gemeine eberesche",
    `gemeine hainbuchegemeine weis` = "gemeine hainbuche",
    gingko = "ginkgo",
    `hollaendische linde` = "holländische linde",
    `kaiser linde` = "kaiserlinde",
    robinie = "scheinakazie",
    `scheinakazie 'bessoniana'` = "scheinakazie",
    `strassenakazie 'monophylla'` = "straßenakazie 'monophylla'",
    suesskirsche = "süßkirsche",
    traubeneicheeiche = "traubeneiche",
    trauerweissbirkehaengebirke = "trauerweissbirke",
    weissdorn = "weißdorn"
)

trees <- trees %>%
        mutate(species_de = recode(.$species_de, !!!tree_key))

# fix encoding issues
Encoding(trees$species_de) <- "UTF-8"

usethis::use_data(trees, overwrite = TRUE)



